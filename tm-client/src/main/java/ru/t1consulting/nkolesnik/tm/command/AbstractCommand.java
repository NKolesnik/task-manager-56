package ru.t1consulting.nkolesnik.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.model.ICommand;
import ru.t1consulting.nkolesnik.tm.api.service.ITokenService;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

@Component
public abstract class AbstractCommand implements ICommand {

    @Setter
    @Getter
    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public String getArgument() {
        return null;
    }

    public abstract void execute();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    @Nullable
    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @NotNull
    @Override
    public String toString() {
        String result = "";
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        if (name != null && !name.isEmpty()) {
            result += name + " : ";
        }
        if (argument != null && !argument.isEmpty()) {
            result += argument + " : ";
        }
        if (description != null && !description.isEmpty()) {
            result += description;
        }
        return result;
    }
}
