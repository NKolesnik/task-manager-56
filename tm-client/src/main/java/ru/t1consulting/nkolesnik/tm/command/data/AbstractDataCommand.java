package ru.t1consulting.nkolesnik.tm.command.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IDomainEndpoint;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;

@Component
@Getter
@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JAXB_XML = "./data_jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data_jaxb.json";

    @NotNull
    public static final String FILE_FASTERXML_XML = "./data_fasterxml.xml";

    @NotNull
    public static final String FILE_FASTERXML_JSON = "./data_fasterxml.json";

    @NotNull
    public static final String FILE_FASTERXML_YAML = "./data_fasterxml.yaml";

    @NotNull
    public static final String APPLICATION_JSON = "application/json";

    @NotNull
    public static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY =
            "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    @Autowired
    public IDomainEndpoint domainEndpoint;

}
