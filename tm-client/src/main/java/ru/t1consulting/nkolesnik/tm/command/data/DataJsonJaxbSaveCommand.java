package ru.t1consulting.nkolesnik.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataJsonJaxbSaveRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

@Component
public final class DataJsonJaxbSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-jaxb-json";

    @NotNull
    public static final String DESCRIPTION = "Save data to JSON file using JAXB.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataJsonJaxb(new DataJsonJaxbSaveRequest(getToken()));
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
