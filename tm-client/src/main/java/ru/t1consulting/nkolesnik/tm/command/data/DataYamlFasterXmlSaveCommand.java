package ru.t1consulting.nkolesnik.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataYamlFasterXmlSaveRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

@Component
public final class DataYamlFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-fasterxml-yaml";

    @NotNull
    public static final String DESCRIPTION = "Save data to YAML file using FasterXML.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlFasterXmlSaveRequest(getToken()));
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
