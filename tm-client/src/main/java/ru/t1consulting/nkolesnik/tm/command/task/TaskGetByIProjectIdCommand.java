package ru.t1consulting.nkolesnik.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskGetByProjectIdRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskGetByProjectIdResponse;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskGetByIProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task by project id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @NotNull final TaskGetByProjectIdResponse response = getTaskEndpoint().getTaskByProjectId(request);
        @Nullable List<TaskDTO> tasks = response.getTasks();
        if (tasks == null || tasks.isEmpty()) return;
        renderTasks(tasks);
    }

}
