package ru.t1consulting.nkolesnik.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskUnbindFromProjectResponse;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

@Component
public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-unbind-from-project";

    @NotNull
    public static final String DESCRIPTION = "Unbind task from project";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        @NotNull final TaskUnbindFromProjectResponse response = getProjectTaskEndpoint().unbindTaskFromProject(request);
    }

}
