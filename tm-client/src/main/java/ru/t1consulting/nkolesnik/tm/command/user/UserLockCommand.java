package ru.t1consulting.nkolesnik.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLockRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLockResponse;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

@Component
public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-lock";

    @NotNull
    public static final String DESCRIPTION = "Lock user in system...";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);
        @NotNull final UserLockResponse response = getUserEndpoint().lockUser(request);
        if (!response.getSuccess()) throw new UserNotFoundException();
        System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
