package ru.t1consulting.nkolesnik.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLogoutRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

@Component
public class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "Log out to system.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
