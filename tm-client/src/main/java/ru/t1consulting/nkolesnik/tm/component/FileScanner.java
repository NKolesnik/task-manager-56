package ru.t1consulting.nkolesnik.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.service.ICommandService;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class FileScanner {

    @NotNull
    private final File folder = new File("./");
    @NotNull
    private final List<String> commands = new ArrayList<>();
    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    @Autowired
    public Bootstrap bootstrap;
    @NotNull
    @Autowired
    private ICommandService commandService;

    private void init() {
        @NotNull final Iterable<AbstractCommand> commands = commandService.getCommandsWithArguments();
        commands.forEach(e -> this.commands.add(e.getName()));
        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    @SneakyThrows
    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean checkName = commands.contains(fileName);
            if (checkName) {
                file.delete();
                bootstrap.processCommand(fileName);
            }
        }
    }

    public void start() {
        init();
    }

    public void stop() {
        executorService.shutdown();
    }

}
