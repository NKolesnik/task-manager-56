package ru.t1consulting.nkolesnik.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.ICommandRepository;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Repository
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();
    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String name = command.getName();
        if (name != null && !name.isEmpty()) mapByName.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) mapByArgument.put(argument, command);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return mapByName.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(@NotNull final String argument) {
        return mapByArgument.get(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArguments() {
        return mapByArgument.values();
    }
}
