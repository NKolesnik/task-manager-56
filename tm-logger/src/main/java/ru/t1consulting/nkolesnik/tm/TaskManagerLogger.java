package ru.t1consulting.nkolesnik.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1consulting.nkolesnik.tm.component.Bootstrap;
import ru.t1consulting.nkolesnik.tm.config.LoggerConfiguration;

public class TaskManagerLogger {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                LoggerConfiguration.class
        );
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run();
    }

}
