package ru.t1consulting.nkolesnik.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.ProjectDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDTO> {

    void add(@Nullable ProjectDTO project);

    void add(@Nullable String userId, @Nullable ProjectDTO project);

    void add(@Nullable Collection<ProjectDTO> projects);

    void add(@Nullable String userId, @Nullable Collection<ProjectDTO> projects);

    void set(@Nullable Collection<ProjectDTO> projects);

    void set(@Nullable String userId, @Nullable Collection<ProjectDTO> projects);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    long getSize();

    long getSize(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAll(@Nullable Comparator<ProjectDTO> comparator);

    @NotNull
    List<ProjectDTO> findAll(@Nullable Sort sort);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator<ProjectDTO> comparator);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    ProjectDTO findById(@Nullable String id);

    @Nullable
    ProjectDTO findById(@Nullable String userId, @Nullable String id);

    boolean existsById(@Nullable String id);

    boolean existsById(@Nullable String userId, @Nullable String id);

    void update(@Nullable ProjectDTO project);

    void updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear();

    void clear(@Nullable String userId);

    void remove(@Nullable ProjectDTO project);

    void remove(@Nullable String userId, @Nullable ProjectDTO project);

    void removeById(@Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

}
