package ru.t1consulting.nkolesnik.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

import java.util.Collection;
import java.util.List;

public interface IUserDtoService extends IDtoService<UserDTO> {

    void add(@Nullable UserDTO user);

    void addALl(@Nullable Collection<UserDTO> users);

    void set(@Nullable Collection<UserDTO> users);

    @NotNull
    UserDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    UserDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    long getSize();

    @Nullable
    List<UserDTO> findAll();

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    );

    void update(@Nullable UserDTO user);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(String login);

    void clear();

    void remove(@Nullable UserDTO user);

    void removeByLogin(@Nullable String login);

    void removeById(@Nullable String id);

}
