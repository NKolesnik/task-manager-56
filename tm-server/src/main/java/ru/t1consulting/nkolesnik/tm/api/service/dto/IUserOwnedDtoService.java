package ru.t1consulting.nkolesnik.tm.api.service.dto;

import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDTO;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDTO> extends IDtoService<M> {

}
