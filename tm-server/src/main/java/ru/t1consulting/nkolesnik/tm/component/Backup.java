package ru.t1consulting.nkolesnik.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.service.IDomainService;
import ru.t1consulting.nkolesnik.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Getter
@Component
public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    @NotNull
    @Autowired
    private IDomainService domainService;

    public void save() {
        getDomainService().saveDataBackup();
    }

    public void load() {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        getDomainService().loadDataBackup();
    }

    public void start() {

    }

    public void stop() {
        executorService.shutdown();
    }

}
