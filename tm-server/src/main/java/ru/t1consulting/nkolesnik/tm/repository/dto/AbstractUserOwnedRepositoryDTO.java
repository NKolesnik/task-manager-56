package ru.t1consulting.nkolesnik.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

@Getter
@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends
        AbstractRepositoryDTO<M> implements IUserOwnedRepositoryDTO<M> {

    @Override
    public abstract void add(@Nullable String userId, @Nullable M modelDTO);

    @Override
    public abstract long getSize(@Nullable String userId);

    @NotNull
    @Override
    public abstract List<M> findAll(@Nullable String userId);

    @Nullable
    @Override
    public abstract M findById(@Nullable String userId, @Nullable String id);

    @Override
    public abstract boolean existsById(@Nullable String userId, @Nullable String id);

    @Override
    public abstract void clear(@Nullable String userId);

    @Override
    public abstract void remove(@Nullable String userId, @Nullable M modelDTO);

    @Override
    public abstract void removeById(@Nullable String userId, @Nullable String id);
}
