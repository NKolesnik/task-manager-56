package ru.t1consulting.nkolesnik.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;

import java.util.Comparator;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public class TaskRepositoryDTO extends AbstractUserOwnedRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    @Override
    public void add(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (userId == null || userId.isEmpty()) return;
        if (task == null) return;
        task.setUserId(userId);
        entityManager.persist(task);
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable final String id) {
        return entityManager.
                createQuery("SELECT t FROM TaskDTO t WHERE t.id = :id", TaskDTO.class).
                setParameter("id", id).
                setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.
                createQuery("SELECT t FROM TaskDTO t WHERE t.id = :id AND t.userId = :userId", TaskDTO.class).
                setParameter("id", id).
                setParameter("userId", userId).
                setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return entityManager.createQuery("FROM TaskDTO", TaskDTO.class).
                setHint("org.hibernate.cacheable", true).
                getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class).
                setHint("org.hibernate.cacheable", true).
                setParameter("userId", userId).
                getResultList();
    }

    @NotNull
    public List<TaskDTO> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM TaskDTO t ORDER BY :sort DESC", TaskDTO.class).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    public List<TaskDTO> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM TaskDTO t ORDER BY :sort DESC", TaskDTO.class).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @NotNull
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY :sort DESC", TaskDTO.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY :sort DESC", TaskDTO.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) {
        return entityManager.
                createQuery("SELECT t FROM TaskDTO t " +
                        "WHERE t.userId = :userId AND t.projectId = :projectId", TaskDTO.class).
                getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.
                createQuery("SELECT t FROM TaskDTO t " +
                        "WHERE t.userId = :userId AND t.projectId = :projectId", TaskDTO.class).
                setParameter("projectId", projectId).
                setParameter("userId", userId).
                getResultList();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.
                createQuery("SELECT COUNT(1) = 1 FROM TaskDTO t WHERE t.id = :id", Boolean.class).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.
                createQuery("SELECT COUNT(1) = 1 FROM TaskDTO t WHERE t.id = :id AND userId = :userId", Boolean.class).
                setParameter("userId", userId).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(t) FROM TaskDTO t", Long.class).getSingleResult();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT COUNT(t) FROM TaskDTO t WHERE t.userId = :userId", Long.class).
                setParameter("userId", userId).
                getSingleResult();
    }

    @Override
    public void remove(@Nullable final TaskDTO task) {
        entityManager.remove(task);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (task == null) return;
        @Nullable final TaskDTO repositoryTask = this.findById(userId, task.getId());
        if (repositoryTask == null) return;
        entityManager.remove(repositoryTask);
    }

    @Override
    public void removeById(@Nullable final String id) {
        @Nullable final TaskDTO task = findById(id);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @Nullable final TaskDTO repositoryTask = this.findById(userId, id);
        if (repositoryTask == null) return;
        entityManager.remove(repositoryTask);
    }

    @Override
    public void removeByProjectId(@Nullable final String projectId) {
        entityManager.
                createQuery("DELETE FROM TaskDTO t WHERE t.projectId = :projectId").
                setParameter("projectId", projectId).
                executeUpdate();
    }

    @Override
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager.
                createQuery("DELETE FROM TaskDTO t WHERE t.projectId = :projectId AND t.userId = :userId").
                setParameter("projectId", projectId).
                setParameter("userId", userId).
                executeUpdate();
    }

    @Override
    public void clear() {
        for (@NotNull final TaskDTO task : findAll()) {
            entityManager.remove(task);
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        for (@NotNull final TaskDTO task : findAll(userId)) {
            entityManager.remove(task);
        }
    }
}
