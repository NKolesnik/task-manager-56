package ru.t1consulting.nkolesnik.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDTO;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.List;

@Getter
@Repository
@Scope("prototype")
public class UserRepositoryDTO extends AbstractRepositoryDTO<UserDTO> implements IUserRepositoryDTO {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        return entityManager.
                createQuery("SELECT u FROM UserDTO u WHERE u.login = :login", UserDTO.class).
                setParameter("login", login).
                setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        return entityManager.
                createQuery("SELECT u FROM UserDTO u WHERE u.email = :email", UserDTO.class).
                setHint("org.hibernate.cacheable", true).
                setParameter("email", email).
                setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        return entityManager.
                createQuery("SELECT COUNT (1) = 1 FROM UserDTO u WHERE u.login = :login", Boolean.class).
                setParameter("login", login).
                getSingleResult();
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        return entityManager.
                createQuery("SELECT COUNT (1) = 1 FROM UserDTO u WHERE u.email = :email", Boolean.class).
                setParameter("email", email).
                getSingleResult();
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        entityManager.merge(user);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        entityManager.merge(user);
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) return;
        @Nullable final UserDTO user = findById(id);
        if (user == null) return;
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        entityManager.merge(user);
    }

    @Override
    public void setPassword(@Nullable final UserDTO user, @Nullable final String password) {
        if (user == null) return;
        if (password == null || password.isEmpty()) return;
        @Nullable final UserDTO repositoryUser = findById(user.getId());
        if (repositoryUser == null) return;
        repositoryUser.setPasswordHash(
                HashUtil.salt(password, propertyService.getPasswordSecret(), propertyService.getPasswordIteration())
        );
        entityManager.merge(repositoryUser);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        @Nullable final UserDTO user = this.findByLogin(login);
        if (user == null) return;
        entityManager.remove(user);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(u) FROM UserDTO u", Long.class).getSingleResult();
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager.createQuery("SELECT u FROM UserDTO u", UserDTO.class).getResultList();
    }

    @Nullable
    @Override
    public UserDTO findById(@Nullable final String id) {
        return entityManager.
                createQuery("SELECT u FROM UserDTO u WHERE u.id = :id", UserDTO.class).
                setParameter("id", id).
                getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.contains(id);
    }

    @Override
    public void clear() {
        for (@NotNull final UserDTO user : findAll()) {
            entityManager.remove(user);
        }
    }

    @Override
    public void remove(@Nullable final UserDTO user) {
        entityManager.remove(user);
    }

    @Override
    public void removeById(@Nullable final String id) {
        @Nullable final UserDTO user = this.findById(id);
        if (user == null) return;
        entityManager.remove(user);
    }

}
