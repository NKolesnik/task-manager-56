package ru.t1consulting.nkolesnik.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.api.repository.model.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Comparator;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Nullable
    @Override
    public Task findById(@Nullable final String id) {
        return entityManager.find(Task.class, id);
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.
                createQuery("SELECT t FROM Task t WHERE t.id = :id AND t.user.id = :userId", Task.class).
                setParameter("id", id).
                setParameter("userId", userId).
                setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("FROM Task", Task.class).
                setHint("org.hibernate.cacheable", true).
                getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT t FROM Task t WHERE t.user.id = :userId", Task.class).
                setHint("org.hibernate.cacheable", true).
                setParameter("userId", userId).
                getResultList();
    }

    @NotNull
    public List<Task> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM Task t ORDER BY :sort DESC", Task.class).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    public List<Task> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM Task t ORDER BY :sort DESC", Task.class).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @NotNull
    public List<Task> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY :sort DESC", Task.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(sort.getComparator())).
                getResultList();
    }

    @NotNull
    public List<Task> findAll(@Nullable final String userId, @Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return entityManager.
                createQuery("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY :sort DESC", Task.class).
                setParameter("userId", userId).
                setParameter("sort", getSortColumnName(comparator)).
                getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String projectId) {
        return entityManager.
                createQuery("SELECT t FROM Task t " +
                        "WHERE t.user.id = :userId AND t.project.id = :projectId", Task.class).
                getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return entityManager.
                createQuery("SELECT t FROM Task t " +
                        "WHERE t.user.id = :userId AND t.project.id = :projectId", Task.class).
                setParameter("userId", userId).
                setParameter("projectId", projectId).
                getResultList();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return entityManager.
                createQuery("SELECT COUNT(1) = 1 FROM Task t WHERE t.id = :id", Boolean.class).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return entityManager.
                createQuery("SELECT COUNT(1) = 1 FROM Task t WHERE t.id = :id AND t.user.id = :userId", Boolean.class).
                setParameter("userId", userId).
                setParameter("id", id).
                getSingleResult();
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(t) FROM Task t", Long.class).getSingleResult();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        return entityManager.
                createQuery("SELECT COUNT(t) FROM Task t WHERE t.user.id = :userId", Long.class).
                setParameter("userId", userId).
                getSingleResult();
    }

    @Override
    public void remove(@Nullable Task task) {
        entityManager.remove(task);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final Task task) {
        if (task == null) return;
        @Nullable final Task repositoryTask = this.findById(userId, task.getId());
        if (repositoryTask == null) return;
        entityManager.remove(repositoryTask);
    }

    @Override
    public void removeById(@Nullable final String id) {
        @Nullable final Task task = findById(id);
        if (task == null) return;
        entityManager.remove(task);
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        @Nullable final Task repositoryTask = this.findById(userId, id);
        if (repositoryTask == null) return;
        entityManager.remove(repositoryTask);
    }

    @Override
    public void removeByProjectId(@Nullable final String projectId) {
        entityManager.
                createQuery("DELETE FROM Task t WHERE t.project.id = :projectId").
                setParameter("projectId", projectId).
                executeUpdate();
    }

    @Override
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        entityManager.
                createQuery("DELETE FROM Task t WHERE t.project.id = :projectId AND t.user.id = :userId").
                setParameter("projectId", projectId).
                setParameter("userId", userId).
                executeUpdate();
    }

    @Override
    public void clear() {
        for (@NotNull final Task task : findAll()) {
            entityManager.remove(task);
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        for (@NotNull final Task task : findAll(userId)) {
            entityManager.remove(task);
        }
    }

}
