package ru.t1consulting.nkolesnik.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.service.IDomainService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IProjectDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserDtoService;
import ru.t1consulting.nkolesnik.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Base64;

@Service
public class DomainService implements IDomainService {

    @NotNull
    public static final String TASKS_TABLE = "tasks";

    @NotNull
    public static final String PROJECTS_TABLE = "projects";

    @NotNull
    public static final String USERS_TABLE = "users";

    @NotNull
    public static final String TASKS_TABLE_BACKUP = "tasks_backup";

    @NotNull
    public static final String PROJECTS_TABLE_BACKUP = "projects_backup";

    @NotNull
    public static final String USERS_TABLE_BACKUP = "users_backup";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_JAXB_XML = "./data_jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "./data_jaxb.json";

    @NotNull
    public static final String FILE_FASTERXML_XML = "./data_fasterxml.xml";

    @NotNull
    public static final String FILE_FASTERXML_JSON = "./data_fasterxml.json";

    @NotNull
    public static final String FILE_FASTERXML_YAML = "./data_fasterxml.yaml";

    @NotNull
    public static final String APPLICATION_JSON = "application/json";

    @NotNull
    public static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY =
            "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String FILE_BACKUP = "./backup.base64";

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setUsers(userService.findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        projectService.set(domain.getProjects());
        taskService.set(domain.getTasks());
        userService.set(domain.getUsers());
    }

    @Override
    @SneakyThrows
    public void loadDataBackup() {
        @NotNull final byte[] base64Bytes = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @Nullable final String base64Data = new String(base64Bytes);
        @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBackup() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BACKUP);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataBase64() {
        @NotNull final byte[] base64Bytes = Files.readAllBytes(Paths.get(FILE_BASE64));
        @Nullable final String base64Data = new String(base64Bytes);
        @Nullable final byte[] bytes = Base64.getDecoder().decode(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBase64() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataBinary() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataBinary() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataJsonFasterXml() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataJsonFasterXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataJsonJaxb() {
        System.setProperty(JAVAX_XML_BIND_CONTEXT_FACTORY, ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY);
        @NotNull final File file = new File(FILE_JAXB_JSON);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataJsonJaxb() {
        System.setProperty(JAVAX_XML_BIND_CONTEXT_FACTORY, ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY);
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, APPLICATION_JSON);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_JSON);
        marshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataXmlFasterXml() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataXmlFasterXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String json = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_XML);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataXmlJaxb() {
        @NotNull final File file = new File(FILE_JAXB_XML);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataXmlJaxb() {
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JAXB_XML);
        marshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataYamlFasterXml() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_YAML)));
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
        @NotNull final Domain domain = yamlMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveDataYamlFasterXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
        @NotNull final String json = yamlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_YAML);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDatabaseBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlSaveUserData = "SELECT * INTO \"" + USERS_TABLE_BACKUP + "\" FROM \"users\";";
        @NotNull final String sqlSaveProjectData = "SELECT * INTO \"" + PROJECTS_TABLE_BACKUP + "\" FROM \"projects\";";
        @NotNull final String sqlSaveTaskData = "SELECT * INTO \"" + TASKS_TABLE_BACKUP + "\" FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveUserData);
            statement.executeUpdate(sqlSaveProjectData);
            statement.executeUpdate(sqlSaveTaskData);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void loadDatabaseBackup() {
        @NotNull final String userName = propertyService.getDatabaseUsername();
        @NotNull final String userPassword = propertyService.getDatabasePassword();
        @NotNull final String url = propertyService.getDatabaseConnectionString();
        @NotNull final Connection connection = DriverManager.getConnection(url, userName, userPassword);
        connection.setAutoCommit(false);
        @NotNull final String sqlClearUserTable = "DELETE FROM \"users\";";
        @NotNull final String sqlLoadUserData = "INSERT INTO \"users\" (SELECT * FROM \"" + USERS_TABLE_BACKUP + "\");";
        @NotNull final String sqlDropBackupUserTable = "DROP TABLE \"" + USERS_TABLE_BACKUP + "\";";
        @NotNull final String sqlClearProjectTable = "DELETE FROM \"projects\";";
        @NotNull final String sqlLoadProjectData = "INSERT INTO \"projects\" (SELECT * FROM \"" + PROJECTS_TABLE_BACKUP + "\");";
        @NotNull final String sqlDropBackupProjectTable = "DROP TABLE \"" + PROJECTS_TABLE_BACKUP + "\";";
        @NotNull final String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        @NotNull final String sqlLoadTaskData = "INSERT INTO \"tasks\" (SELECT * FROM \"" + TASKS_TABLE_BACKUP + "\");";
        @NotNull final String sqlDropBackupTaskTable = "DROP TABLE \"" + TASKS_TABLE_BACKUP + "\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearUserTable);
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlLoadUserData);
            statement.executeUpdate(sqlLoadProjectData);
            statement.executeUpdate(sqlLoadTaskData);
            statement.executeUpdate(sqlDropBackupTaskTable);
            statement.executeUpdate(sqlDropBackupProjectTable);
            statement.executeUpdate(sqlDropBackupUserTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}