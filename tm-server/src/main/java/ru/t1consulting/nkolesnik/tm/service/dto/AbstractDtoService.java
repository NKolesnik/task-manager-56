package ru.t1consulting.nkolesnik.tm.service.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractModelDTO;

@Service
public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IRepositoryDTO<M>> implements IDtoService<M> {

    @Autowired
    protected ApplicationContext context;

    protected abstract IRepositoryDTO<M> getRepository();

}
