package ru.t1consulting.nkolesnik.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDTO;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepositoryDTO<M>>
        extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

    protected abstract IUserOwnedRepositoryDTO<M> getRepository();

}
