package ru.t1consulting.nkolesnik.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.t1consulting.nkolesnik.tm.api.service.dto.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDTO;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.repository.dto.TaskRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.*;

@Service
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO, ITaskRepositoryDTO> implements ITaskDtoService {

    protected ITaskRepositoryDTO getRepository() {
        return context.getBean(TaskRepositoryDTO.class);
    }

    @Override
    public void add(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.add(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.add(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final Collection<TaskDTO> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            for (TaskDTO task : tasks)
                repository.add(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final Collection<TaskDTO> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            for (TaskDTO task : tasks)
                repository.add(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void set(@Nullable final Collection<TaskDTO> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.clear();
            for (TaskDTO task : tasks)
                repository.add(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void set(@Nullable final String userId, @Nullable final Collection<TaskDTO> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.clear();
            for (TaskDTO task : tasks)
                repository.add(userId, task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        @NotNull final TaskDTO task = new TaskDTO();
        try {
            em.getTransaction().begin();
            task.setName(name);
            task.setUserId(userId);
            repository.add(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        @NotNull final TaskDTO task = new TaskDTO();
        try {
            em.getTransaction().begin();
            task.setName(name);
            task.setUserId(userId);
            task.setDescription(description);
            repository.add(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        @NotNull final TaskDTO task = new TaskDTO();
        try {
            em.getTransaction().begin();
            task.setName(name);
            task.setUserId(userId);
            task.setDescription(description);
            task.setDateBegin(dateBegin);
            task.setDateEnd(dateEnd);
            repository.add(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
        return task;
    }

    @Override
    public long getSize() {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.getSize();
        } finally {
            em.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @Nullable final List<TaskDTO> tasks;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            tasks = repository.findAll();
            if (tasks.isEmpty()) return Collections.emptyList();
            return tasks;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<TaskDTO> tasks;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            tasks = repository.findAll(userId);
            if (tasks.isEmpty()) return Collections.emptyList();
            return tasks;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final Comparator<TaskDTO> comparator) {
        if (comparator == null) return findAll();
        @Nullable final List<TaskDTO> taskList;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            taskList = repository.findAll(comparator);
            if (taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @Nullable final List<TaskDTO> taskList;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            taskList = repository.findAll(sort);
            if (taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Comparator<TaskDTO> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @Nullable final List<TaskDTO> taskList;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            taskList = repository.findAll(userId, comparator);
            if (taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final List<TaskDTO> taskList;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            taskList = repository.findAll(userId, sort);
            if (taskList.isEmpty()) return Collections.emptyList();
            return taskList;
        } finally {
            em.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        if (projectId == null || projectId.isEmpty()) return findAll(userId);
        @Nullable List<TaskDTO> result;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            result = repository.findAllByProjectId(userId, projectId);
            if (result.isEmpty()) return Collections.emptyList();
            return result;
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.findById(id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.findById(userId, id);
        } finally {
            em.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.existsById(id);
        } finally {
            em.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            em.close();
        }
    }

    @Override
    public void update(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.update(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            @Nullable final TaskDTO task = repository.findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    @SneakyThrows
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (status == null) throw new StatusNotFoundException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            @Nullable final TaskDTO task = repository.findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(status);
            repository.update(task);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.clear();
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.clear(userId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final String id = task.getId();
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final String id = task.getId();
            repository.removeById(userId, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.removeById(userId, id);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager em = repository.getEntityManager();
        try {
            em.getTransaction().begin();
            repository.removeByProjectId(userId, projectId);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

}
