package ru.t1consulting.nkolesnik.tm.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1consulting.nkolesnik.tm.api.repository.model.IRepository;
import ru.t1consulting.nkolesnik.tm.api.service.model.IService;
import ru.t1consulting.nkolesnik.tm.model.AbstractModel;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @Autowired
    protected ApplicationContext context;

    protected abstract IRepository<M> getRepository();

}
